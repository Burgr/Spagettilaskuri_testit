﻿using NUnit.Framework;
using TDDHarjoitus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilausTesti
{
    [TestFixture]
    public class KeilausTest
    {
        [Test]
        public void KaikkiNolla()
        {
            Keilaus nollat = new Keilaus();
            for (int i = 0; i < 20; i++)
                nollat.heitto(0);
            Assert.AreEqual(0, nollat.pisteet);
        }

        [Test]
        public void KaikkiKaatoja()
        {
            Keilaus kaadot = new Keilaus();
            for (int i = 0; i < 10; i++)
                kaadot.heitto(10);
            Assert.AreEqual(300, kaadot.pisteet);
        }

        [Test]
        public void SatunnaisiaIlmanPaikkojaTaiKaatoja()
        {
            Keilaus random = new Keilaus();
            Assert.That(random.testi("0,9,8,1,7,2,6,4,5,4,4,3,3,5,7,2,3,0,0,1"), Is.EqualTo(74));
        }
    }
}
