﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDDHarjoitus
{
    public class Keilaus
    {
        public int pisteet { get; set; }
        public void heitto(int tötsät)
        {            
            if (tötsät == 10) { pisteet = pisteet + tötsät * 3; }
            else pisteet = tötsät + pisteet;        
        }

        public int testi(string test)
        {
            string[] heitot = test.Split(',');
            int total = 0;
            foreach (var heitto in heitot)
            {
                total += int.Parse(heitto);
            }
            return total;
        }
        
        static void Main(string[] args)
        {
        }
    }
}
